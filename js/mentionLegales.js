//menu hamburger {
var content_hamburger = document.querySelector('.nav_imgCategories');
var sidebarBody = document.querySelector(".hamburger-sidebar-body");
var button = document.querySelector("#hamburger-button");
var overlay = document.querySelector(".hamburger-overlay");
var activedClass = "hamburger-activated";

sidebarBody.innerHTML = content_hamburger.innerHTML;

button.addEventListener("click", function(e) {
    e.preventDefault();
    this.parentNode.parentNode.parentNode.classList.add(activedClass);
})

// button.addEventListener("keydown", function(e) {
//     if (this.parentNode.parentNode.classList.contains(activedClass)) {
//         if(e.repeat === false && e.which === 27) {
//             alert(hello);
//             this.parentNode.parentNode.classList.remove(activedClass);
//         } 
//     }
// })

overlay.addEventListener('click', function(e){
    e.preventDefault();

    this.parentNode.classList.remove(activedClass);
})