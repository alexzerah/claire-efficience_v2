$(document).ready(function(){

    //Slick carousel
    $('.s4-carousel').slick({
        dots: true,
        dotsClass: "slick-dots" ,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<img class="arrow arrow-left" src="img/utilities/arrow-left.svg">',
        nextArrow: '<img class="arrow arrow-right" src="img/utilities/arrow-right.svg">'
    });

    //hover carousel
    $(".s4-carousel-container-hover").hover(
        function() {
            jQuery(".s4-box-expert").fadeOut("slow");
            setTimeout(function(){
                jQuery(".s4-carousel-box-hover").fadeIn("slow")
            }, 0);
        }, function() {
            setTimeout(function(){
                jQuery(".s4-box-expert").fadeIn("slow")
            },0);
            jQuery(".s4-carousel-box-hover").fadeOut("slow");
        }
    )

    //height section 1 equalt to window height
    $(".section1").height(($(window).height()));
    $(window).resize(function(){
        $(".section1").height(($(window).height()));
    });

    //menu hamburger {
        var content_hamburger = document.querySelector('.nav_imgCategories');
        var sidebarBody = document.querySelector(".hamburger-sidebar-body");
        var button = document.querySelector("#hamburger-button");
        var overlay = document.querySelector(".hamburger-overlay");
        var activedClass = "hamburger-activated";

        sidebarBody.innerHTML = content_hamburger.innerHTML;

        button.addEventListener("click", function(e) {
            e.preventDefault();
            this.parentNode.parentNode.parentNode.classList.add(activedClass);
        })

        // button.addEventListener("keydown", function(e) {
        //     if (this.parentNode.parentNode.classList.contains(activedClass)) {
        //         if(e.repeat === false && e.which === 27) {
        //             alert(hello);
        //             this.parentNode.parentNode.classList.remove(activedClass);
        //         } 
        //     }
        // })

        overlay.addEventListener('click', function(e){
            e.preventDefault();

            this.parentNode.classList.remove(activedClass);
        })

});