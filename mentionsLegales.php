<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Claire - Mentions Légales</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href='css/mentionLegales.css'>
    <link rel="icon" type="image/png" href="img/favicon.png" />
</head>
<body>
<nav>
    <div class="nav_imgCategories">
        <a href="index.php">
            <img src="img/logo.png" class="mL-logo" alt="logo">
        </a>
        <a href="index.php">
            <img src="img/logoColor.png" class="mL-logo2" alt="logo">
        </a>
        <ul class="nav_categories">
            <a href="index.php#produitLZ" class="scroll"><li>PRODUIT</li></a>
            <a href="index.php#bienfaitsLZ" class="scroll"><li>BIENFAITS</li></a>
            <a href="index.php#avisLZ" class="scroll"><li>AVIS D'EXPERTS</li></a>
        </ul>
    </div>
    
    <div class="hamburger-button-container">
        <button id="hamburger-button">&#9776</button>
    </div>
    <div class="nav_rs">
        <a href="https://www.facebook.com/SprayClaire/" target="_blank">
            <i class="fa fa-facebook" aria-hidden="true"></i>
        </a>
        <a href="https://twitter.com/Spray_Claire" target="_blank">
            <i class="fa fa-twitter" aria-hidden="true"></i>
        </a>
        <a href="https://www.linkedin.com/company/spray-claire/" target="_blank" >
            <i class="fa fa-linkedin" aria-hidden="true"></i>
        </a>
        <a href="https://www.pinterest.fr/sprayclaire" target="_blank">
            <i class="fa fa-pinterest-p" aria-hidden="true"></i>
        </a>
    </div>
</nav>
<div class="hamburger-sidebar">
            <div class="hamburger-sidebar-header"></div>
            <div class="hamburger-sidebar-body"></div>
        </div>
        <div class="hamburger-overlay"></div>
<div class="container">

    <section class="mL-s1">
        <h2 class="mL-h2">
            Mentions légales
        </h2>
        <p>Dénomination sociale : </p>
        <p>Claire</p>
        ​
        <p>Forme juridique :</p>
        <p>SAS</p>
        ​
        <p>Siège social :</p>
        <p>126 boulevard haussmann</p>
        <p>75008 PARIS</p>
        ​
        <p>Capital social :</p>
        <p>10 000€</p>
        ​
        <p>Numéro d'inscription au RCS : en cours d'attribution</p>
        ​
        <p>Email :</p>
        <p>contact@claire-depolluant.com</p>
        ​
        ​
        <p>Responsable publication :</p>
        <p>Roman Jauneaux</p>
        ​
        <p>Informations concernant l'hébergeur :</p>
        <p>LWS</p>
        <p>LWS.fr</p>
        <p>Téléphone : 01 77 62 3003</p>
        <p>4, Rue Galvani </p>
        <p>75838 PARIS CEDEX 17, France</p>
    </section>
</div>

<script
src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<script src="../js/mentionLegales.js"></script>
</body>
</html>