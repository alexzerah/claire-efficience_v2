<?php
include(dirname(__FILE__).'/inc/config.php');
?>
<?php 
if (isset($_POST['submit'])) {
    if (!empty($_POST['email'])) {
         echo($_POST['email']);
         $email = htmlspecialchars($_POST["email"]);
         $req = $bdd->prepare("INSERT INTO claire_contacts (email) VALUES (:email)");
         $req->execute(array(
             "email" => $email
         ));
        $subject = "Merci pour votre précommande - claire-depolluant.com";
        $message = "Votre précommande a bien été enregistrée. Nous vous remercions de l'interet que vous portez à notre produit.\n Cordialement,\n l'équipe Claire.";
        $header = "From: \"contac@claire-depolluant.com\"<contact@claire-depolluant.com>";
        mail( $_POST['email'] , $subject , $message , $header);
    } 
    header('Location: index.php');
    exit;
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110522930-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-110522930-1');
    </script>
    <!---->

    <!-- Hotjar Tracking Code for https://claire-depolluant.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:709895,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:image" content="https://claire-depolluant.com/img/photospray.png" >
    <meta name="description" content="Le spray dépolluant Claire supprime 95% de la pollution toxique autour de vous. Précommande gratuite" >
    <meta name="twitter:card" content="summary" >
    <meta name="twitter:site" content="@spray_claire" >
    <meta name="twitter:url" content="https://claire-depolluant.com/" >
    <meta name="twitter:title" content="Claire - Spray anti-pollution" >
    <meta name="twitter:description" content="Le spray dépolluant Claire supprime 95% de la pollution toxique autour de vous. Précommande gratuite." />
    <meta name="twitter:image" content="https://claire-depolluant.com/img/photospray.png" />
    <title>Claire - Spray anti-pollution</title>
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/style.css">

</head>
<body>
<header class="section1">
    <video class="s1-video_parallax" loop mute autoplay src="video/video_section1.mp4"></video>
    <img src="img/ImgParallaxS3.jpg" class="s1-img_parallax" alt="champs d'herbe">
    <div class="section1-content">
        <nav>
            <div class="nav_imgCategories">
                <!-- <img src="img/logo.png" class="logo" alt="logo"> -->
                <ul class="nav_categories">
                    <a href="#produitLZ" class="scroll navLZ"><li class="sliding-u-l-r-l">PRODUIT</li></a>
                    <a href="#bienfaitsLZ" class="scroll navLZ"><li class="sliding-u-l-r-l">BIENFAITS</li></a>
                    <a href="#avisLZ" class="scroll navLZ"><li class="sliding-u-l-r-l">AVIS D'EXPERTS</li></a>
                </ul>
            </div>
            <div class="hamburger-button-container">
                <button id="hamburger-button">&#9776</button>
            </div>
            <div class="nav_rs">
                <a href="https://www.facebook.com/SprayClaire/" target="_blank">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a href="https://twitter.com/Spray_Claire" target="_blank">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a href="https://www.linkedin.com/company/spray-claire/" target="_blank" >
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a>
                <a href="https://www.pinterest.fr/sprayclaire" target="_blank">
                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                </a>
            </div>
        </nav>
        
        <div class="hamburger-sidebar">
            <div class="hamburger-sidebar-header"></div>
            <div class="hamburger-sidebar-body"></div>
        </div>
        <div class="hamburger-overlay"></div>
        <div class="s1-content">
            <div class="s1-container-box">
                <img class="s1-logo" src="img/logo.png" alt="logo Claire">
                <h1 id="titre">CLAIRE</h1>
                <h2 id="sousTitre">Le spray anti-pollution</h2>
            </div>
        </div>
    </div>
    <a href="#produitLZ" class="scroll"><div class="scroll-down svg" id="home-scroll-down">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_2" x="0px" y="0px" viewBox="0 0 25.166666 37.8704414" enable-background="new 0 0 25.166666 37.8704414" xml:space="preserve" >
            <path class="stroke" fill="none" stroke="#c7c4b8" stroke-width="2.5" stroke-miterlimit="10" d="M12.5833445 36.6204414h-0.0000229C6.3499947 36.6204414 1.25 31.5204487 1.25 25.2871208V12.5833216C1.25 6.3499947 6.3499951 1.25 12.5833216 1.25h0.0000229c6.2333269 0 11.3333216 5.0999947 11.3333216 11.3333216v12.7037992C23.916666 31.5204487 18.8166714 36.6204414 12.5833445 36.6204414z"></path>
            <path class="scroller" fill="#c7c4b8" d="M13.0833359 19.2157116h-0.9192753c-1.0999985 0-1.9999971-0.8999996-1.9999971-1.9999981v-5.428606c0-1.0999994 0.8999987-1.9999981 1.9999971-1.9999981h0.9192753c1.0999985 0 1.9999981 0.8999987 1.9999981 1.9999981v5.428606C15.083334 18.315712 14.1833344 19.2157116 13.0833359 19.2157116z"></path>
        </svg>
        <i class="icon icon-arrow-down"></i></a>
    </div>
</header>
<section class="section2" id="produitLZ">
    <div class="s2-containerImg">
        <img class="s2-imgProduit" src="img/spraysansbouchon.png" alt="produit spray Claire">
    </div>
    <div class="s2-container2">
        <div class="s2-c2-container">
            <p class="s2-c2-p1">SPRAY<br>CLAIRE</p>
            <p class="s2-c2-p2">Pensé par une mère et créé par des experts, Claire est un spray révolutionnaire qui élimine la pollution de l’air autour de vous.</br></br>
            Elaboré à base de végétaux ayant des vertus dépolluantes, il élimine jusqu’à 95% des particules fines polluantes autour de vous, en intérieur comme en extérieur.</p>
            <div class="s2-box-cta">
                <p class="s2-c2-p5">Précommandez gratuitement le produit</p>
                <form class="s2-box-cta-in" method="post" action="" onsubmit="validateForm()">
                    <input type="email" class="s2-inputMail" name="email" placeholder="Votre email...">
                    <button class="s2-sendEmail" type="submit" name="submit" id="submit">ENVOYER</button>
                </form>
            </div>
        </div>
    </div>
    
</section>
<section class="section3" id="bienfaitsLZ">
    <div class="s3s4-imgParallax">
    </div>
    <div class="s3-container-txt">
        <!-- <div class="s3-rect-filter"></div> -->
        <div class="s3-txt">
            <p class="s3-p1">BIENFAITS</p>
            <div class="s3-container-box">
                <div class="s3-box">
                    <div clas="s3-box-chiffreText">
                        <img src="img/Ampoule_Icon.png" class="icons1">
                        <p class="s3-box-p2">UN SPRAY</br>REVOLUTIONNAIRE</p>
                    </div>
                    <p class="s3-box-p3">Contrairement aux sprays assainissants classiques, le spray anti-pollution de Claire élimine jusqu’à 95% de la pollution, même en extérieur !</p>
                </div>
                <div class="s3-box">
                    <img src="img/Bouclier_Icon.png" class="icons2">
                    <p class="s3-box-p2">PROTECTEUR</p>
                    <p class="s3-box-p3">Le spray anti-pollution éradique instantanément les particules fines polluantes qui affectent votre santé au quotidien.</p>
                </div>
                <div class="s3-box">
                    <img src="img/Molecule_Icon.png" class="icons3">
                    <p class="s3-box-p2">ATTESTE</br>SCIENTIFIQUEMENT</p>
                    <p class="s3-box-p3">Le spray anti-pollution de Claire, créé en collaboration avec des experts de la santé, est reconnu comme un produit de qualité, 3 fois plus efficace qu’un purificateur classique.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section4" id="avisLZ">
    <h3 class="s4-h3">
        AVIS D'EXPERTS
    </h3>
    <div class="s4-container-carousel">
        <div class="s4-carousel">
            <div class="s4-carousel-container-hover">
                <div class="s4-carousel-box-hover">
                    <img class="s4-carousel-quote-hover" src="img/quotes-hover.svg" alt="icones de guillemets">
                    <p class="s4-carousel-hover-p">Grâce à sa formule hautement dépolluante formulé à base de chlorophytum, spathiphyllum et draceana marginata, ce spray est d'une efficacité incontestable</p>
                </div>
                <div class="s4-box-expert s4-box-expert1">
                    <img class="s4-expert-img" src="img/expert1.png" alt="image de l'expert 1">
                    <div class="s4-box-quote-svg">
                        <img class="s4-carousel-quote" src="img/quotes.svg" alt="icone guillemet">
                    </div>
                    <p class="s4-expert-name">Docteur Beaucour</p>
                    <p class="s4-expert-comment">"Grâce à sa formule hautement dépolluante..."</p>
                </div>
            </div>
            <div class="s4-carousel-container-hover">
                     <div class="s4-carousel-box-hover">
                    <img class="s4-carousel-quote-hover" src="img/quotes-hover.svg" alt="icones de guillemets">
                    <p class="s4-carousel-hover-p">Enfin un produit qui répond au au problème inquiétant de la pollution de l’air en ville.</p>
                </div>
                <div class="s4-box-expert s4-box-expert2">
                    <img class="s4-expert-img" src="img/expert2.png" alt="image de l'expert 2">
                    <div class="s4-box-quote-svg2">
                        <img class="s4-carousel-quote" src="img/quotes.svg" alt="icone de guillemets">
                    </div>
                    <p class="s4-expert-name">Docteur Rose</p>
                    <p class="s4-expert-comment">"Enfin un produit qui répond au au problème..."</p>
                </div>
            </div>
            <div class="s4-carousel-container-hover">
                <div class="s4-carousel-box-hover">
                    <img class="s4-carousel-quote-hover" src="img/quotes-hover.svg" alt="icones de guillemets">
                    <p class="s4-carousel-hover-p">La suppression des particules fines polluantes en intérieur diminue de 70% le risque de maladies respiratoire. Un spray plus qu’utile donc : essentiel.</p>
                </div>
                <div class="s4-box-expert s4-box-expert3">
                    <img class="s4-expert-img" src="img/expert3.png" alt="image de l'expert 3">
                    <div class="s4-box-quote-svg3">
                    <img class="s4-carousel-quote" src="img/quotes.svg" alt="icone de guillemets">
                    </div>
                    <p class="s4-expert-name">Docteur Hammou</p>
                    <p class="s4-expert-comment">"La suppression des particules fines polluantes..."</p>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <p><a href="mentionsLegales.php"  target="_blank">Mentions Légales</a> - <a href="http://bit.ly/KitPresse"  target="_blank">Kit Presse</a> <br class="footer-br"> - <a href="mailto:contact@claire-depolluant.com"  target="_blank">contact@claire-depolluant.com</a></p>
</footer>
<script
src="https://code.jquery.com/jquery-3.2.1.min.js"
integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
crossorigin="anonymous"></script>
<script src="js/slick.min.js"></script>
<script src="https://unpkg.com/sweetalert2"></script>
<script src="js/main.js"></script>
<script>
function validateForm() {
    swal({
        title: 'Votre précommande a bien été prise en compte',
        type: 'success',
        timer: 4500,
        showConfirmButton: false,
        showCloseButton: true
    })
}

// $('#submit').submit(function() {
//     $.ajax({
//         url: 'index.php',
//         type: 'POST',
//         data: {
//             email: 'email@example.com',
//             message: 'hello world!'
//         },
//         success: function(msg) {
//             alert('Email Sent');
//         } 
//     })
    
// })
</script>
<script type="text/javascript" src="js/smoothScroll.js"></script>  
</body>
</html>

